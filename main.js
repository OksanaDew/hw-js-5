//Экранирование - это изменение семантики символов для движка js через добавление перед ним обратного слэша.
// Такие символы со слэшом называют спецсимволы и они позволяют выводить на страницу или в консоль:
// кавычки, перенос строки, табуляцию, тот же обратный слэш и т.д.//

function createNewUser() {
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();
    let now = "0" + day + "." + "0" + ++month + "." + year;
    let newUser = {
        firstName: "",
        lastName: "",
        birthday: "",
        toLog: function getLogin(){
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        toCountAge: function getAge() {
            let age = Number;
            if (+now.substring(3,5) > +this.birthday.substring(3,5)) {
                age = +now.substring(6) - +this.birthday.substring(6);
            } else if (+now.substring(3,5) === +this.birthday.substring(3,5)) {
                if (+now.substring(0,2) > +this.birthday.substring(0,2)) {
                    age = +now.substring(6) - +this.birthday.substring(6);
                } else {
                    age = (+now.substring(6) - +this.birthday.substring(6)) - 1;
                }
            } else {
                age = (+now.substring(6) - +this.birthday.substring(6)) - 1;
            }
            return age;
        },
        toPassword: function getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6);
        }
    };

    do {
        newUser.firstName = prompt('Enter first name:');
        newUser.lastName = prompt('Enter last name:');
        newUser.birthday = prompt('Enter birthday dd.mm.yyyy:');
    } while (newUser.lastName === " " && newUser.firstName === " ");
    console.log(newUser.toLog());
    console.log(newUser.toCountAge());
    console.log(newUser.toPassword());
    return newUser.firstName + " " + newUser.lastName;
}
console.log(createNewUser());